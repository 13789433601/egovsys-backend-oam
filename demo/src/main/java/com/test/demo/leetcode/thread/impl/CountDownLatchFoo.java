package com.test.demo.leetcode.thread.impl;

import java.util.concurrent.CountDownLatch;

/**
 * @Author: wangyf
 * @Date: 2019/11/24 16:17
 */
public class CountDownLatchFoo {
	private CountDownLatch countDownLatch1;
	private CountDownLatch countDownLatch2;

	public CountDownLatchFoo(){
		countDownLatch1 = new CountDownLatch(1);
		countDownLatch2 = new CountDownLatch(1);
	}
	public void first(Runnable printFirst) throws InterruptedException {
		printFirst.run();
		countDownLatch1.countDown();
	}
	public void second(Runnable printSecond) throws InterruptedException {
		countDownLatch1.await();
		printSecond.run();
		countDownLatch2.countDown();
	}
	public void third(Runnable printThird) throws InterruptedException {
		countDownLatch2.await();
		printThird.run();
	}
}
