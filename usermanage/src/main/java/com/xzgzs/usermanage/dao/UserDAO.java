package com.xzgzs.usermanage.dao;

import java.io.Serializable;

/**
 * @Author: wangyf
 * @Date: 2019/6/2 14:52
 */
//@Entity
//@Table(name = "user", schema = "apiuser")
public class UserDAO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String userId;
	private String username;
	private String password;
	private String userType;
	private Integer createTime;
	private Integer updateTime;
	private String phone;
	private String email;
	private String wechat;
	private String alipay;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public Integer getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Integer createTime) {
		this.createTime = createTime;
	}

	public Integer getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Integer updateTime) {
		this.updateTime = updateTime;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public String getAlipay() {
		return alipay;
	}

	public void setAlipay(String alipay) {
		this.alipay = alipay;
	}
//	@Id
//	@Column(name = "user_id", nullable = false, length = 64)
//	public String getUserId() {
//		return userId;
//	}
//
//	public void setUserId(String userId) {
//		this.userId = userId;
//	}
//
//	@Basic
//	@Column(name = "username", nullable = false, length = 64)
//	public String getUsername() {
//		return username;
//	}
//
//	public void setUsername(String username) {
//		this.username = username;
//	}
//
//	@Basic
//	@Column(name = "password", nullable = false, length = 64)
//	public String getPassword() {
//		return password;
//	}
//
//	public void setPassword(String password) {
//		this.password = password;
//	}
//
//	@Basic
//	@Column(name = "user_type", nullable = true, length = 1)
//	public String getUserType() {
//		return userType;
//	}
//
//	public void setUserType(String userType) {
//		this.userType = userType;
//	}
//
//	@Basic
//	@Column(name = "create_time", nullable = true)
//	public Integer getCreateTime() {
//		return createTime;
//	}
//
//	public void setCreateTime(Integer createTime) {
//		this.createTime = createTime;
//	}
//
//	@Basic
//	@Column(name = "update_time", nullable = true)
//	public Integer getUpdateTime() {
//		return updateTime;
//	}
//
//	public void setUpdateTime(Integer updateTime) {
//		this.updateTime = updateTime;
//	}
//
//	@Basic
//	@Column(name = "phone", nullable = true, length = 16)
//	public String getPhone() {
//		return phone;
//	}
//
//	public void setPhone(String phone) {
//		this.phone = phone;
//	}
//
//	@Basic
//	@Column(name = "email", nullable = true, length = 64)
//	public String getEmail() {
//		return email;
//	}
//
//	public void setEmail(String email) {
//		this.email = email;
//	}
//
//	@Basic
//	@Column(name = "wechat", nullable = true, length = 64)
//	public String getWechat() {
//		return wechat;
//	}
//
//	public void setWechat(String wechat) {
//		this.wechat = wechat;
//	}
//
//	@Basic
//	@Column(name = "Alipay", nullable = true, length = 64)
//	public String getAlipay() {
//		return alipay;
//	}
//
//	public void setAlipay(String alipay) {
//		this.alipay = alipay;
//	}

}
