package com.xzgzs.netty.test.online;

import com.xzgzs.netty.test.MyClientInitalizer;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 描述 :
 *
 * @author wangyf
 * @create 2019/12/03
 */
public class MyChatClient {
	
	public static void main(String[] args) throws InterruptedException {
		EventLoopGroup eventExecutors = new NioEventLoopGroup();
		
		try {
			Bootstrap bootstrap = new Bootstrap();
			bootstrap.group(eventExecutors).channel(NioSocketChannel.class).handler(new MyChatClientInitalizer());
			
			Channel channel = bootstrap.connect("localhost", 8899).sync().channel();
//			ChannelFuture channelFuture = bootstrap.connect("localhost", 8899);
//			channelFuture.channel().closeFuture().sync();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			for (;;) {
				channel.writeAndFlush(br.readLine() + "\r\n");
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			eventExecutors.shutdownGracefully();
		}
	}
}
