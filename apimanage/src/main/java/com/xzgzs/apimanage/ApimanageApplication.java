package com.xzgzs.apimanage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ApimanageApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApimanageApplication.class, args);
	}

}
