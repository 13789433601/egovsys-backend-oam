package com.xzgzs.apimanage.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @Author: wangyf
 * @Date: 2019/11/7 15:57
 */
@Mapper
public interface TestDao {
	@Select("insert into test(id,name,phone)values(#{id},#{name},#{phone})")
	void test(@Param("id")String id,@Param("name")String name, @Param("phone")String phone);

	@Select("select id,name,phone from test limit 10000,10")
	List<Map> slowQuery();
}
