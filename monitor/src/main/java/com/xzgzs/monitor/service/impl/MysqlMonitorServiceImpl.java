package com.xzgzs.monitor.service.impl;

import com.xzgzs.commen.util.FileReaderUtils;
import com.xzgzs.monitor.mapper.MysqlMonitorMapper;
import com.xzgzs.monitor.service.MysqlMonitorService;
import com.xzgzs.monitor.utils.SerialBlobs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

/**
 * @Author: wangyf
 * @Date: 2019/11/19 15:56
 */
@Service
public class MysqlMonitorServiceImpl implements MysqlMonitorService {
	@Autowired
	private MysqlMonitorMapper mysqlMonitorMapper;
	@Autowired
	private SerialBlobs serialBlobs;
	@Override
	public List findSlowQuery() throws IOException, SQLException {
		List<Map> list = mysqlMonitorMapper.findSlowQuery();

		for (int i = 0; i <list.size(); i++) {
			Object sql_text = list.get(i).get("sql_text");
			byte[] bytes = serialBlobs.getSerial(sql_text);
//			System.out.println(new String(bytes, "UTF-8").substring(27));
			list.get(i).put("sql_text", new String(bytes, "UTF-8").substring(27));
		}
		return list;
	}

	@Override
	public List findSlowQueryFile() throws IOException {
		FileReaderUtils fileReaderUtils = new FileReaderUtils();
		List list = fileReaderUtils.readerLine("D:\\test.txt");
		return list;
	}
}
