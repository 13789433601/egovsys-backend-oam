package com.xzgzs.usermanage.config;

import com.xzgzs.usermanage.dto.UserDTO;
import com.xzgzs.usermanage.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * @Author: wangyf
 * @Date: 2019/6/2 15:32
 */
public class CustomUserService implements UserDetailsService {

	@Autowired
	private LoginService loginService;
	@Override
	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
		UserDTO userDTO = new UserDTO();
		userDTO = loginService.findUserByUserName(s);
		return userDTO;
	}
}
