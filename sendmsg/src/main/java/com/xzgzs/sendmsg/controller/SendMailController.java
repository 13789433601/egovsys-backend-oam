package com.xzgzs.sendmsg.controller;

import com.xzgzs.commen.util.ResultUtils;
import com.xzgzs.commen.annotion.ErrorLoggerPrint;
import com.xzgzs.sendmsg.util.SendMsgUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;

/**
 * @Author: wangyf
 * @Date: 2019/6/11 18:00
 */
@RestController
@CrossOrigin
public class SendMailController {
	@Autowired
	private SendMsgUtil sendMsgUtil;

	/**
	 * 发送邮件服务
	 * @param emailAddr 邮件发送的地址
	 * @return 发送成功或者失败
	 */
	@ErrorLoggerPrint("发送邮件测试注解")
	@RequestMapping(value = "/sendMail",method = RequestMethod.GET)
	public ResultUtils sendMail(@RequestParam String emailAddr) {
		ResultUtils resultUtils = new ResultUtils();
		try {
			sendMsgUtil.sendMsg(emailAddr);
		} catch (MessagingException e) {
//			e.printStackTrace();
			resultUtils.setCode(ResultUtils.INTERNAL_SERVER_ERROR);
			resultUtils.setMsg("发送失败，请点击重新发送！");
			return resultUtils;
		}
		resultUtils.setCode(ResultUtils.OK);
		resultUtils.setMsg("发送成功，请注意查收");
		return resultUtils;
	}
}
