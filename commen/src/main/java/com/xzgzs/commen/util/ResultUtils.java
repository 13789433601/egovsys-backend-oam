package com.xzgzs.commen.util;


import java.io.Serializable;

/**
 * @Author: wangyf
 * @Date: 2019/5/27 22:59
 */
public class ResultUtils implements Serializable {
	/**
	 * 成功回调
	 */
	public static final int OK = 200;
	/**
	 * 无请求头
	 */
	public static final int UNAUTHORIZED = 401;
	/**
	 * 未找到路径
	 */
	public static final int NOT_FOUND = 404;
	/**
	 * 服务器连接错误
	 */
	public static final int INTERNAL_SERVER_ERROR = 500;
	/**
	 * 当服务器无法识别请求的方法，并且无法支持其对任何资源的请求。
	 */
	public static final int  NOT_IMPLEMENTED = 501;


	/**
	 * 返回码
	 */
	private int code;
	/**
	 * 返回信息
	 */
	private String msg;
	/**
	 * 返回值
	 */
	public Object data;

	public ResultUtils(int code, String msg, Object data){
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public ResultUtils() {
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
