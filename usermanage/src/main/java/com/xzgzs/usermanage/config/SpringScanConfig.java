package com.xzgzs.usermanage.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: wangyf
 * @Date: 2019/5/31 1:09
 */
@Configuration
@ComponentScan(value = "common")
public class SpringScanConfig  {
}
