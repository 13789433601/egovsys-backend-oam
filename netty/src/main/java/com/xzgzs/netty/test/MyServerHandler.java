package com.xzgzs.netty.test;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import java.util.UUID;

/**
 * 描述 :
 *
 * @author wangyf
 * @create 2019/12/03
 */
public class MyServerHandler extends SimpleChannelInboundHandler<String>{
	@Override
	protected void channelRead0(ChannelHandlerContext channelHandlerContext, String msg) throws Exception {
		System.out.println(channelHandlerContext.channel().remoteAddress() + ", " + msg);
		
		channelHandlerContext.writeAndFlush("from server " + UUID.randomUUID());
	}
	
	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
		cause.printStackTrace();
//		super.exceptionCaught(ctx, cause);
		ctx.close();
	}
	
}
