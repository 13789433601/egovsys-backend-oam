package com.test.demo;

import org.springframework.util.StringUtils;

import java.util.Arrays;

/**
 * @Author: wangyf
 * @Date: 2019/10/6 21:59
 */
public class Sum {
	public static void main(String[] args) {
//		int[] arry = {1,2,3,4,5,6,7,8,9,10};
//		int target = 13;
//		int[] num = twoSum(arry, target);
//		for (int i = 0; i < num.length; i++) {
//			System.out.println(num[i]);
//		}
		String s = "   =-123 ffs34 -1231 ";
		System.out.println(findIntByString(s));
	}

	/**
	 * 两个数相加和为某个数时的问题
	 * @param nums 数组
	 * @param target 和数
	 * @return 返会两个数的下标
	 */
	public static int[] twoSum(int[] nums, int target) {
		int indexArrayMax = 2047;
		int[] indexArrays = new int[indexArrayMax + 1];
		for (int i = 0; i < nums.length; i++) {
			int diff = target - nums[i];
			int index = diff & indexArrayMax;
			if (indexArrays[index] != 0) {
				return new int[] { indexArrays[index] - 1, i };
			}
			indexArrays[nums[i] & indexArrayMax] = i + 1;
		}
		throw new IllegalArgumentException("No two sum value");
	}

	/**
	 * 翻转
	 * @param str
	 * @return
	 */
	public static int findIntByString (String str) {
		char[] c = str.toCharArray();
		StringBuffer num = new StringBuffer();
		for (int i = 0; i < c.length; i++) {
			if (Character.isDigit(c[i])) {
				if ( i-1 >=0 && StringUtils.isEmpty(num.toString()) && "-".equals(String.valueOf(c[i-1]))){
					num.append(c[i-1]);
				}
				num.append(c[i]);
			}
			if (!Character.isDigit(c[i]) && !StringUtils.isEmpty(num.toString()) ){
				return Integer.parseInt(num.toString());
			}
		}
		return 0;
	}
	
	
	
	/**
	 * 1、2、3 ----100
	 * @return删除掉奇数位的数。最后一位数是什么
	 * @param array 需要筛选的数据
	 */
	public static int[] iteaterArray(int[] array) {
		//新建数组，准备放删除奇数位数据之后的数
		int[] a = {};
		//循环遍历数组
		for (int j = 0; j < array.length; j++) {
			//位置在偶数位的数据，放到新的数组中
			if ((j + 1) % 2 == 0) {
				//数组扩容，每次加一
				a = Arrays.copyOf(a, a.length + 1);
				//将符合条件的数据放到数组中
				a[a.length - 1] = array[j];
			}
		}
		//如果最后数据只剩下一个数据，则该数据就是我们需要的最终数据
		if (array.length == 1) {
			return array;
		}else {
			//递归数据，将新的数组作为参数
			return iteaterArray(a);
		}
	}
}
