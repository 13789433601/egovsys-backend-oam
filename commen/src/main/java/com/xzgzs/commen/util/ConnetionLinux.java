package com.xzgzs.commen.util;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * @Author: wangyf
 * @Date: 2019/11/21 11:20
 */
public class ConnetionLinux {
	public static void main(String[] args) throws IOException {
		String host = "192.168.199.183";
		int port = 22;
		String username = "root";
		String pwd = "wangyf";
		connect(host, port, username,pwd );
	}
	/**
	 * 远程连接Linux 服务器 来获取文件
	 *
	 *
	 */
	public static void connect(String host, int port, String username, String pwd) throws IOException {

		// 创建连接
		Connection conn = new Connection(host, port);
		// 启动连接
		conn.connect();
		// 验证用户密码
		conn.authenticateWithPassword(username, pwd);
		Session session = conn.openSession();
		session.execCommand("cd /var/lib/mysql; cat localhost-slow.log;");

		// 消费所有输入流
		String inStr = consumeInputStream(session.getStdout());
		String errStr = consumeInputStream(session.getStderr());
		System.out.println(inStr+"..............."+errStr);
		session.close();
		conn.close();
	}
	/**
	 *   消费inputstream，并返回
	 */
	public static String consumeInputStream(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String s ;
		StringBuilder sb = new StringBuilder();
		while((s=br.readLine())!=null){
			System.out.println(s+"==================================");
			sb.append(s);
		}
		return sb.toString();
	}

}
