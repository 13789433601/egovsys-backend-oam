package com.xzgzs.netty.test.online;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

/**
 * 描述 :
 *
 * @author wangyf
 * @create 2019/12/03
 */
public class MyChatClientHandler extends SimpleChannelInboundHandler<String> {
	@Override
	protected void channelRead0(ChannelHandlerContext channelHandlerContext, String msg) throws Exception {
		System.out.println(msg);
	}
}
