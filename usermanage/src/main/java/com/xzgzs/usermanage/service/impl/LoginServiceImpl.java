package com.xzgzs.usermanage.service.impl;

import com.xzgzs.commen.util.ResultUtils;
import com.xzgzs.usermanage.dao.UserDAO;
import com.xzgzs.usermanage.dto.UserDTO;
import com.xzgzs.usermanage.mapper.LoginMapper;
import com.xzgzs.usermanage.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

import static com.xzgzs.commen.util.ResultUtils.OK;

/**
 * @Author: wangyf
 * @Date: 2019/5/27 23:30
 */
@Service
public class LoginServiceImpl implements LoginService {
	@Autowired
	private LoginMapper loginMapper;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	@Override
	public ResultUtils login(String username, String password) {
		ResultUtils resultUtils = new ResultUtils();
		Map map = new HashMap(16);
		UserDAO userDAO = loginMapper.findUserInfo(username);
		String pwd = userDAO.getPassword();
		boolean b = bCryptPasswordEncoder.matches(password,pwd);
		if (b) {
			resultUtils.setCode(OK);
			resultUtils.setMsg("登录成功");

		}
		return resultUtils;
	}

	@Override
	public UserDTO findUserByUserName(String s) {
		UserDTO userDTO = loginMapper.findUserByUserName(s);
		return userDTO;
	}
}
