package com.xzgzs.usermanage.mapper;

import com.xzgzs.usermanage.dto.UserDTO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author: wangyf
 * @Date: 2019/5/26 14:07
 */
@Mapper
public interface UserMapper {
	 void saveUser(UserDTO userDTO);

}
