package com.test.demo;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author: wangyf
 * @Date: 2019/9/14 13:16
 */
public class test {
	private static volatile Integer state = new Integer(1);

	public static void main(String[] args) {
//		String str ;
//		Map map = new HashMap(16);
//		List list = new ArrayList(16);
//		int[] nums = {1,2,3,3,5,5,6,6};
//		removeDuplicates(nums);

//		int i = 7;
//		do {
//			System.out.println(--i);
//			--i;
//		} while (i != 0);
//		System.out.println(i);

//		int i = 0;
//		for (foo('A');foo('B') && (i<2); foo('C')){
//			i++;
//			foo('D');
//		}
		//打印线程，分别打印10次ABC
//		Object lock = new Object();
//		ExecutorService executorService = new ThreadPoolExecutor(3, 5, 0, TimeUnit.MILLISECONDS, new LinkedBlockingDeque<>());
//		executorService.execute(new Worker("A", 10, lock));
//		executorService.execute(new Worker("B", 10, lock));
//		executorService.execute(new Worker("C", 10, lock));
//		try {
//			Thread.sleep(10);
//		} catch (InterruptedException e) {
//			e.printStackTrace();
//		}finally {
//			executorService.shutdown();
//		}
		
		
		System.out.println(test1());
	}

	public static class Worker implements Runnable {
		private String key;
		private Object lock;
		private int count;

		public Worker(String key, int count, Object lock) {
			this.key = key;
			this.count = count;
			this.lock = lock;
		}
		@Override
		public void run() {
			for (int i = 0; i < count; i++) {
				if (key == "A") {
					synchronized (lock) {
						while (state != 1) {
							try {
								lock.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						System.out.println(i + ", " + key);
						state = 2;
						lock.notifyAll();
					}
				} else if (key == "B") {
					synchronized (lock) {
						while (state != 2) {
							try {
								lock.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						System.out.println(i + ", " + key);
						state = 3;
						lock.notifyAll();
					}
				} else if (key == "C") {
					synchronized (lock) {
						while (state != 3) {
							try {
								lock.wait();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
						System.out.println(i + ", " + key);
						state = 1;
						lock.notifyAll();
					}
				}

			}
		}
	}

	public static boolean foo(char C) {
		System.out.println(C);
		return true;
	}

	private  void removeDuplicates(int[] nums) {

	}
	
	
	
	public static int test1() {
		int i = 10;
		try {
			i++;
			if (i/0 == 10) {
				i++;
			}
			++i;
		}catch (Exception e) {
			i++;
		}
		
		return i++;
	}
	
	
}
