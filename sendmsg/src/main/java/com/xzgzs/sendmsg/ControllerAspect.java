package com.xzgzs.sendmsg;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.UUID;

/**
 * 定义切面，获取输入输出信息
 * @Author: wangyf
 * @Date: 2019/8/13 14:01
 */
@Aspect
@Component
public class ControllerAspect {
	public static final Logger logger = LoggerFactory.getLogger(ControllerAspect.class);

	/**
	 * 定义切面
	 */
	@Pointcut("execution(public * com.xzgzs.sendmsg.controller.*.*(..))")
	public void executService() {
	}

	/**
	 * 切面获取信息
	 * @param proceedingJoinPoint 参数
	 * @return 信息
	 * @throws Throwable 错误
	 */
    @Around("executService()")
	public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		Signature signature = proceedingJoinPoint.getSignature();
		String uuid = UUID.randomUUID().toString();
		long startTime = System.currentTimeMillis();
		Object result = proceedingJoinPoint.proceed();
		long endTime = System.currentTimeMillis();
		float lastTime = (endTime - startTime) * 1.000000f / 1000;
		logger.warn("Service方法耗时==[{}.{}]执行标识：{},开始时间：{},结束时间：{},方法用时{}", new Object[]{signature.getDeclaringTypeName(), signature.getName(), uuid, new Timestamp(startTime), new Timestamp(endTime), lastTime});
		return result;
	}

}
