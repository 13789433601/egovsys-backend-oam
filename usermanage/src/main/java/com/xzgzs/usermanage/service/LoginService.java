package com.xzgzs.usermanage.service;

import com.xzgzs.commen.util.ResultUtils;
import com.xzgzs.usermanage.dto.UserDTO;

/**
 * @Author: wangyf
 * @Date: 2019/5/27 23:22
 */
public interface LoginService {


	ResultUtils login(String username, String password);

	UserDTO findUserByUserName(String s);
}
