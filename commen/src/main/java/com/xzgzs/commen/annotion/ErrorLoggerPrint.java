package com.xzgzs.commen.annotion;

import java.lang.annotation.*;

/**
 * 第一次测试用的annotion
 * @author wangyf
 * @date 20190927 22:34
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface ErrorLoggerPrint {
	String value() default "";
}
