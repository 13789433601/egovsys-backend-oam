package com.test.demo.leetcode.leetcode0001_0100;

import java.util.Arrays;

/**
 * 描述 :
 *
 * @author wangyf
 * @create 2019/11/06
 */
public class LeectCode1_10 {
	
	
	public static void main(String[] args) {
		isPalindrome(1221);
	}
	
	
	
	//TODO 1 两数之和
	/**
	 * 两数之和
	 *
	 * 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
	 你可以假设每种输入只会对应一个答案。但是，你不能重复利用这个数组中同样的元素。
	 示例:
	 给定 nums = [2, 7, 11, 15], target = 9
	 因为 nums[0] + nums[1] = 2 + 7 = 9
	 所以返回 [0, 1]
	 
	 解题思路，1.暴力法 ： 双层for循环
	 * @param nums 数组
	 * @param target 和
	 * @return 两束相加和为target的数组下标
	 */
	public int[] twoSum(int[] nums, int target) {
		int[] clone = new int[0];
		for (int i=0; i<nums.length-1; i++) {
			for (int j=1+i; j<nums.length; j++) {
				if (nums[i]+nums[j] == target){
//					System.out.println(i+":"+j);
					//这里是我自己加的，防止有多个数进行相加之和= target
					clone = Arrays.copyOf(clone,clone.length+2);
					clone[clone.length-2] = i;
					clone[clone.length-1] = j;
				}
			}
		}
		return clone;
	}
	
	
	public static boolean isPalindrome(int x) {
		String[] x1 = (""+x).split("");
		int size = x1.length / 2 ;
		for (int i = 0; i < size; i++) {
			if (!x1[i].equals(x1[x1.length-1-i])) {
				return false;
			}
		}
		return true;
	}
}
