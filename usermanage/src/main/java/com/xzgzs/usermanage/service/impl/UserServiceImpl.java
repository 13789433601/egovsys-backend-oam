package com.xzgzs.usermanage.service.impl;

import com.xzgzs.commen.util.DateFormetter;
import com.xzgzs.commen.util.SnowFlake;
import com.xzgzs.usermanage.dto.UserDTO;
import com.xzgzs.usermanage.mapper.UserMapper;
import com.xzgzs.usermanage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;

/**
 * @Author: wangyf
 * @Date: 2019/5/26 14:00
 */
@Service
public class UserServiceImpl implements UserService {
	@Autowired
	private UserMapper userMapper;


	@Transactional(rollbackFor = Exception.class)
	@Override
	public void saveUser(UserDTO userDTO) {
		Long l = SnowFlake.generateId();
		DateFormetter dateFormetter = new DateFormetter();
//		long l = snowFlake.nextId();
		String uid = String.valueOf(l);
		userDTO.setUserId(uid);
		userDTO.setCreateTime(dateFormetter.dateForString(Instant.now().getEpochSecond()));
		userMapper.saveUser(userDTO);
	}
}
