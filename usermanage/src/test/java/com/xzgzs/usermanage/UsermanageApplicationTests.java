package com.xzgzs.usermanage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsermanageApplicationTests {

	@Test
	public void contextLoads() {
//		Assert.notNull(time, "time is null");
		Long time = Instant.now().getEpochSecond();
		DateTimeFormatter ftf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		System.out.println(time);
		System.out.println(String.valueOf(Calendar.getInstance().getTimeInMillis()));
		System.out.println( ftf.format(LocalDateTime.ofInstant(Instant.ofEpochSecond(time),ZoneId.systemDefault())));
	}

}
