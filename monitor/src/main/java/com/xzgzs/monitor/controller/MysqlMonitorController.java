package com.xzgzs.monitor.controller;

import com.xzgzs.commen.util.ResultUtils;
import com.xzgzs.monitor.service.MysqlMonitorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @Author: wangyf
 * @Date: 2019/11/19 15:40
 */
@RestController
@CrossOrigin
@RequestMapping("/mysql")
public class MysqlMonitorController {
	@Autowired
	private MysqlMonitorService mysqlMonitoryService;
	@RequestMapping(value = "/findSlowQuery", method = RequestMethod.GET)
	public ResultUtils findSlowQuery() throws IOException, SQLException {
		ResultUtils resultUtils = new ResultUtils();
		List list = mysqlMonitoryService.findSlowQuery();
		resultUtils.setCode(200);
		resultUtils.setData(list);
		resultUtils.setMsg("慢sql查询成功");
		return resultUtils;
	}

	/**
	 * 按行读取文件中的mysql慢查询数据，按照文件，然后返回
	 * @return 慢查询信息
	 * @throws IOException
	 * @throws SQLException
	 */
	@RequestMapping(value = "/findSlowQueryFile", method = RequestMethod.GET)
	public ResultUtils findSlowQueryFile() throws IOException {
		ResultUtils resultUtils = new ResultUtils();
		List list = mysqlMonitoryService.findSlowQueryFile();
		resultUtils.setCode(200);
		resultUtils.setData(list);
		resultUtils.setMsg("慢sql查询成功");
		return resultUtils;
	}

}
