package com.test.demo.leetcode;

import com.test.demo.leetcode.thread.impl.Foo;

/**
 * 这个类时解决leetCode 中的多线程的方法
 * 所有leetCode 中多线程问题都由这个类来解决
 * @Author: wangyf
 * @Date: 2019/11/23 21:57
 */
public class Thread {
	public static void main(String[] args) {

		Foo foo = new Foo();

	}
}
