package com.xzgzs.usermanage.mapper;

import com.xzgzs.usermanage.dao.UserDAO;
import com.xzgzs.usermanage.dto.UserDTO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户登录用的mapper
 * @Author: wangyf
 * @Date: 2019/5/30 20:42
 */
@Mapper
public interface LoginMapper {
	/**
	 * @param username 用户名
	 * @return UserDTO 用户信息
	 */
	UserDAO findUserInfo(String username);

	UserDTO findUserByUserName(String s);
}
