package com.xzgzs.monitor.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * @Author: wangyf
 * @Date: 2019/11/19 15:51
 */
public interface MysqlMonitorService {
	/**
	 * 查询慢sql接口
	 * @return  慢sql的执行情况
	 */
	List findSlowQuery() throws IOException, SQLException;

	/**
	 * 慢查询sql接口，按照查文件的形式实现
	 * @return 慢查询的sql
	 *
	 */
	List findSlowQueryFile() throws IOException;
}
