package com.xzgzs.apimanage.controller;

import com.xzgzs.apimanage.service.TestService;
import com.xzgzs.commen.util.ResultUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: wangyf
 * @Date: 2019/11/7 15:47
 */
@RestController
@CrossOrigin
public class TestController {
	@Autowired
	private TestService testService;
	@RequestMapping(value = "/test1",method = RequestMethod.GET)
	public ResultUtils test() throws InterruptedException {
		ResultUtils resultUtils = new ResultUtils();
		testService.test();
		resultUtils.setCode(200);
		resultUtils.setMsg("请求成功");

		return resultUtils;
	}
	@RequestMapping(value = "/slowQuery",method = RequestMethod.GET)
	public ResultUtils slowQuery() {
		ResultUtils resultUtils = new ResultUtils();
		List list = testService.slowQuery();
		resultUtils.setCode(200);
		resultUtils.setMsg("请求成功");
		resultUtils.setData(list);
		return resultUtils;
	}

}
