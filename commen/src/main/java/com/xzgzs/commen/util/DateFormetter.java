package com.xzgzs.commen.util;

import org.springframework.stereotype.Component;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

/**
 * @Author: wangyf
 * @Date: 2019/6/11 22:27
 */
@Component
public class DateFormetter {
	/**
	 * 将时间戳转化成时间，时间戳为秒级
	 * @param date
	 * @return
	 */
	public String dateForString(Long date) {
		DateTimeFormatter ftf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		String time = ftf.format(LocalDateTime.ofInstant(Instant.ofEpochSecond(date),ZoneId.systemDefault()));
		return time;
	}
}
