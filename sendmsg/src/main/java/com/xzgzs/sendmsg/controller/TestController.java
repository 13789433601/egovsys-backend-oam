package com.xzgzs.sendmsg.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;

/**
 * @Author: wangyf
 * @Date: 2019/9/28 18:56
 */
@RestController
@CrossOrigin
public class TestController {
	@Autowired
	@Qualifier("getExecutorPool")
	private ExecutorService getExecutorPool;

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test() {
//		getExecutorPool.execute(() -> {
			sou();
//		});
		return "hello, annotation!";
	}

	public void sou() {
		System.out.println(Thread.currentThread().getName() + "-------");
	}
}
