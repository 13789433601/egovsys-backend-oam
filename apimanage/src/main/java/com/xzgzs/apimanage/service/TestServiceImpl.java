package com.xzgzs.apimanage.service;

import com.xzgzs.apimanage.dao.TestDao;
import com.xzgzs.apimanage.utils.ThreadPoolUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * @Author: wangyf
 * @Date: 2019/11/7 15:54
 */
@Service
public class TestServiceImpl implements TestService {
	@Autowired
	private TestDao testDao;
	@Autowired
	private ThreadPoolUtils threadPoolUtils;

	@Override
	public void test() throws InterruptedException {
//		ThreadPoolUtils threadPoolUtils = new ThreadPoolUtils();
		long l = System.currentTimeMillis();

		for (int i =0;i<100000; i++) {
			Runnable runnable = () -> {
				runInsert();
			};
			threadPoolUtils.getPool().execute(runnable);
		}
		System.out.println(System.currentTimeMillis() - l+"************************************************************");

	}

	@Override
	public List slowQuery() {
		List<Map> list = testDao.slowQuery();
		return list;
	}

	public void runInsert() {
		System.out.println(Thread.currentThread().getName());
		String id = UUID.randomUUID().toString();
		String name;
		String phone;
		name = id;
		phone = "123123222";
		testDao.test(id, name, phone);
	}
}
