package com.xzgzs.usermanage.controller;

import com.alibaba.fastjson.JSON;
import com.xzgzs.commen.util.ResultUtils;
import com.xzgzs.usermanage.dto.UserDTO;
import com.xzgzs.usermanage.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import static com.xzgzs.commen.util.ResultUtils.OK;

/**
 * @Author: wangyf
 * @Date: 2019/5/25 23:19
 */
@RestController
@CrossOrigin
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;

	/**
	 * 新增用户，
	 * @param user 新增用户的 数据
	 */
	@RequestMapping(value = "/registUser",method = RequestMethod.POST)
	public void registUser(@RequestBody String user) {
		JSON json = (JSON) JSON.parse(user);
		UserDTO userDTO = JSON.toJavaObject(json, UserDTO.class);
		userService.saveUser(userDTO);
	}

	@RequestMapping(value = "/deleteUser",method = RequestMethod.DELETE)
	public ResultUtils deleteUser(@RequestHeader("Authorization")String auth) {
		return new ResultUtils(OK,"成功",null);
	}
}
