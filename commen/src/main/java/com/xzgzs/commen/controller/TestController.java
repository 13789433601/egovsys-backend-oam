package com.xzgzs.commen.controller;

import com.xzgzs.commen.annotion.ErrorLoggerPrint;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: wangyf
 * @Date: 2019/9/28 18:56
 */
@RestController
@CrossOrigin
public class TestController {
	@ErrorLoggerPrint
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String test() {
		return "hello, annotation!";
	}
}
