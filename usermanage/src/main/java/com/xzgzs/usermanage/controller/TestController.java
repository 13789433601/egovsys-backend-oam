package com.xzgzs.usermanage.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author: wangyf
 * @Date: 2019/5/25 14:15
 */
@RestController
@CrossOrigin
public class TestController {
	@RequestMapping("/test")
	public String test () {
		return "hello world  ";
	}
}
