package com.xzgzs.apimanage.utils;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

import java.util.concurrent.*;

/**
 * @Author: wangyf
 * @Date: 2019/11/7 15:35
 */
@Configuration
@EnableAsync
public class ThreadPoolUtils {
	@Bean
	public ExecutorService getPool() {
		ThreadFactory threadFactory = new ThreadFactoryBuilder()
				.setNameFormat("manage-thread-%d").build();
		ExecutorService executorService = new ThreadPoolExecutor(16,
				64,
				10000,
				TimeUnit.SECONDS,
				new LinkedBlockingDeque<>(1024),
				threadFactory,
				new ThreadPoolExecutor.CallerRunsPolicy());
		return executorService;
	}
}
