package com.xzgzs.sendmsg;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import java.util.concurrent.*;

@SpringBootApplication
@EnableEurekaClient
public class SendmsgApplication {

	public static void main(String[] args) {
		SpringApplication.run(SendmsgApplication.class, args);
	}

	@Bean(name = "getExecutorPool")
	public ExecutorService getPool () {
		//获取当前线程的cpu核心数
		final int cpuNum = Runtime.getRuntime().availableProcessors();
		System.out.println("当前机器的cpu数量为"+cpuNum);
		//创建线程池，， 获线程的名称
		ThreadFactory nameThreadFactory = new ThreadFactoryBuilder().setNameFormat("Thread-name-%d").build();
		ExecutorService executorService = new ThreadPoolExecutor(0,
				cpuNum * 2,
				0L,
				TimeUnit.MILLISECONDS,
				new LinkedBlockingDeque<>(10240),
				nameThreadFactory,
				new ThreadPoolExecutor.CallerRunsPolicy());
		return executorService;
	}
}
