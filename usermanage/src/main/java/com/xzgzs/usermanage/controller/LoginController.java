package com.xzgzs.usermanage.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.xzgzs.commen.util.ResultUtils;
import com.xzgzs.usermanage.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @Author: wangyf
 * @Date: 2019/5/27 23:19
 */
@RestController
@CrossOrigin
@RequestMapping("/manage")
public class LoginController {
	@Autowired
	private LoginService loingService;

	/**
	 * 用户登录功能，可以用用户名密码登录。
	 * @param json
	 * @return
	 */
	@RequestMapping(value = "/login",method = RequestMethod.POST)
	public ResultUtils login(@RequestBody String json) {
		JSONObject jsonObject = JSON.parseObject(json);
		String username = jsonObject.getString("username");
		String password = jsonObject.getString("password");

		//调用登录接口，传入用户名和密码
		ResultUtils resultUtils = loingService.login(username,password);
		return resultUtils;
	}
}
