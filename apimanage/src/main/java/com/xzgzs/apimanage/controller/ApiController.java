package com.xzgzs.apimanage.controller;

import com.xzgzs.apimanage.service.ApiService;
import com.xzgzs.commen.util.ResultUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: wangyf
 * @Date: 2019/6/23 13:45
 */
@RestController
@CrossOrigin
@RequestMapping("/api")
public class ApiController {
	@Autowired
	private ApiService apiService;


	@RequestMapping(value = "/getApi",method = RequestMethod.GET)
	public ResultUtils getApi(String apiname){
		ResultUtils resultUtils = new ResultUtils();
		List list = apiService.findApi();
		resultUtils.setCode(200);
		resultUtils.setMsg("请求成功");
		resultUtils.setData(list);
		return resultUtils;
	}

}
