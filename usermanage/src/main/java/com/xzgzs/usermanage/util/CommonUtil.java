package com.xzgzs.usermanage.util;

/**
 * 常量
 * @Author: wangyf
 * @Date: 2019/6/1 17:08
 */
public class CommonUtil {
	public static final String SECERT = "mySecret";
	public static final String TOEKN_HEADER = "Authorization";
	public static final String TOEKN_HEAD = "Bearer ";
	public static final String CLAIM_KEY_USERNAME = "sub";
	public static final String CLAIM_KEY_CREATED = "created";
	public static final String EXPIRATION = "30*60";


}
