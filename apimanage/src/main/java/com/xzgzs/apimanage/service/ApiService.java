package com.xzgzs.apimanage.service;

import java.util.List;

/**
 * @Author: wangyf
 * @Date: 2019/6/23 19:32
 */
public interface ApiService {
	/**
	 * 查找API接口
	 * @return API
	 */
	List findApi();
}
