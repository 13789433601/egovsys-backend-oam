package com.xzgzs.commen.util;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: wangyf
 * @Date: 2019/11/19 22:29
 */
@Component
public class FileReaderUtils {
	/**
	 * 按行读取文件，返回list集合形式。
	 * @param filePath
	 * @return
	 */
	@Bean
	public List readerLine(String filePath) throws IOException {
		List list = new ArrayList();
		FileReader fileReader = null;
		BufferedReader bufferedReader = null;
		try {
			fileReader = new FileReader(filePath);
			bufferedReader = new BufferedReader(fileReader);
			String str = null;
			while ((str = bufferedReader.readLine()) != null) {
				list.add(str);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			fileReader.close();
			bufferedReader.close();
		}
		return list;
	}

//	/**
//	 * 远程连接Linux 服务器 来获取文件
//	 *
//	 *
//	 */
//	public static void connect(String host, String port, String username, String pwd) {
//		Connection
//	}

}
