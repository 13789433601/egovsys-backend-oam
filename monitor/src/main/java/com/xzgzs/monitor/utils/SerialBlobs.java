package com.xzgzs.monitor.utils;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.Session;
import org.springframework.stereotype.Component;

import java.io.*;

/**
 * @Author: wangyf
 * @Date: 2019/11/19 16:27
 */
@Component
public class SerialBlobs {
	/**
	 * object 类转换成byte[] 实现blob的转换
	 * @param sql_text 数据库中的qsl_text字段
	 * @return byte[] 数组
	 * @throws IOException 抛出异常
	 */
	public byte[] getSerial(Object sql_text) throws IOException {
		byte[] bytes = null;
		ByteArrayOutputStream byteArrayOutputStream = null;
		ObjectOutputStream objectOutputStream = null;
		try {
			byteArrayOutputStream = new ByteArrayOutputStream();
			objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
			objectOutputStream.writeObject(sql_text);
			bytes = byteArrayOutputStream.toByteArray();
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			byteArrayOutputStream.flush();
			byteArrayOutputStream.close();
			objectOutputStream.close();
		}
		return bytes;
	}

	public static void main(String[] args) throws IOException {
		String host = "192.168.199.183";
		int port = 22;
		String username = "root";
		String pwd = "wangyf";
		connect(host, port, username,pwd );
	}
	/**
	 * 远程连接Linux 服务器 来获取文件
	 *
	 *
	 */
	public static void connect(String host, int port, String username, String pwd) throws IOException {

		// 创建连接
		Connection conn = new Connection(host, port);
		// 启动连接
		conn.connect();
		// 验证用户密码
		conn.authenticateWithPassword(username, pwd);
		Session session = conn.openSession();
		session.execCommand("cd /var/lib/mysql; cat localhost-slow.log;");

		// 消费所有输入流
		String inStr = consumeInputStream(session.getStdout());
		String errStr = consumeInputStream(session.getStderr());
		System.out.println(inStr+"..............."+errStr);
		session.close();
		conn.close();
	}
	/**
	 *   消费inputstream，并返回
	 */
	public static String consumeInputStream(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String s ;
		StringBuilder sb = new StringBuilder();
		while((s=br.readLine())!=null){
			System.out.println(s+"==================================");
			sb.append(s);
		}
		return sb.toString();
	}


}
