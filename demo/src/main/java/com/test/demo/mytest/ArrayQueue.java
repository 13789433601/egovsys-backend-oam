package com.test.demo.mytest;

/**
 * 描述 :
 *
 * @author wangyf
 * @create 2019/12/02
 */

public class ArrayQueue {
	//数组
	private String[] queue;
	/**
	 *  数组大小
	 */
	private int size;
	/**
	 * 数组头
	 */
	private int head;
	/**
	 * 数组实际存储容量
	 */
	private int anInt;
	/**
	 * 数组尾
	 */
	private int rear;
	
	public ArrayQueue(int maxSize) {
		this.size = maxSize;
		queue = new String[size];
		anInt = 0;
		head = 0;
		rear = -1;
	}
	
	/**
	 * 判断队列是否为空
	 * @return 如果为空，，则返回true
	 */
	public boolean isEmtry () {
		return (anInt == 0);
	}
	
	/**
	 * 判断队列是否已满
	 * @return 判断队列是否已满
	 */
	public boolean isFull() {
		return (anInt == size);
	}
	
	/**
	 * 队列实际存储数量大小
	 * @return
	 */
	public int size() {
		return anInt;
	}
	
	public void add(String value) {
		if (isFull()) {
			System.out.println("对列已满");
		}
		
		rear = anInt % size;
		queue[rear] = value;
		anInt++;
	}
	
	public String remove() {
		if (isEmtry()) {
			System.out.println("对列为空");
			return null;
		}
		anInt--;
		head = head % size;
		return queue[head++];
	}
	
	
	public String peek() {
		if (isEmtry()) {
			System.out.println("对列为空");
			return null;
		}
		return queue[head];
		
	}
	
	
	public void display() {
		if (isEmtry()) {
			System.out.println("对列为空");
		}
		int item = head;
		for (int i = 0; i < anInt; i++) {
			System.out.println(queue[item++ % size]);
			
		}
		System.out.println(" ");
	}
	
	public static void main(String[] args) {
		ArrayQueue arrayQueue = new ArrayQueue(30);
		arrayQueue.add("3");
		arrayQueue.add("4");
		arrayQueue.add("5");
		arrayQueue.display();
		
		System.out.println(arrayQueue.peek());
		
	}
	
}
