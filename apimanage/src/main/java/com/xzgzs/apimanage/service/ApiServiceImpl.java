package com.xzgzs.apimanage.service;

import com.xzgzs.apimanage.dao.ApiDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: wangyf
 * @Date: 2019/6/23 19:34
 */
@Service
public class ApiServiceImpl implements ApiService {
	@Autowired
	private ApiDao apiDao;
	@Override
	public List findApi() {
		List list  = apiDao.findApi();
		return list;
	}
}
