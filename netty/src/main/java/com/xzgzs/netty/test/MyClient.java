package com.xzgzs.netty.test;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * 描述 :
 *
 * @author wangyf
 * @create 2019/12/03
 */
public class MyClient {
	
	public static void main(String[] args) throws InterruptedException {
		EventLoopGroup eventExecutors = new NioEventLoopGroup();
		
		try {
			Bootstrap bootstrap = new Bootstrap();
			bootstrap.group(eventExecutors).channel(NioSocketChannel.class).handler(new MyClientInitalizer());
			
			ChannelFuture channelFuture = bootstrap.connect("localhost", 8899);
			channelFuture.channel().closeFuture().sync();
			
		}finally {
			eventExecutors.shutdownGracefully();
		}
	}
	
}
