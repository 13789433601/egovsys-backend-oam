package com.xzgzs.apimanage.dao;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @Author: wangyf
 * @Date: 2019/6/23 19:36
 */
@Mapper
public interface ApiDao {

	/**
	 * 获取api接口
	 * @return API列表
	 */
	List findApi();
}
