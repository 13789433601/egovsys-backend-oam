package com.test.demo.leetcode.leetcode0001_0100;

/**
 * 描述 :
 *
 * @author wangyf
 * @create 2019/11/12
 */
public class LeetCode11_20 {
	public static void main(String[] args) {
		//LeetCode 11
//		int[] height = {1,8,6,2,5,4,8,3,7};
//		System.out.println(maxArea(height));
	
		//leetcode 14
		
//		String[] strs = {"zhangsan","zhangsi","zhangwu"};
//		System.out.println(LeetCode21.longestCommonPrefix(strs));
		
		
		intToRoman(3651);
		
	}
	
	/** leetCode 11
	 * 给定 n 个非负整数 a1，a2，...，an，每个数代表坐标中的一个点 (i, ai) 。在坐标内画 n 条垂直线，垂直线 i 的两个端点分别为 (i, ai) 和 (i, 0)。找出其中的两条线，使得它们与 x 轴共同构成的容器可以容纳最多的水。
	 说明：你不能倾斜容器，且 n 的值至少为 2。
	 
	 使用 双指针法 ，
	 * @param height
	 * @return
	 */
	public static int maxArea(int[] height) {
		int maxArea = 0, l = 0, r = height.length - 1;
		while (l<r) {
			//比较
			maxArea = Math.max(maxArea, Math.min(height[l], height[r]) * (r-l));
			if (height[l] > height[r]) {
				r--;
			}else {
				l++;
			}
		}
		
		return maxArea;
	}
	
	
	public static class LeetCode21{
		/**
		 *
		 * leetcode 14
		 * 编写一个函数来查找字符串数组中的最长公共前缀。
		 如果不存在公共前缀，返回空字符串 ""。
		 示例 1:
		 输入: ["flower","flow","flight"]
		 输出: "fl"
		 示例 2:
		 输入: ["dog","racecar","car"]
		 输出: ""
		 解释: 输入不存在公共前缀。
		 说明:
		 所有输入只包含小写字母 a-z 。
		 
		 * 解题思路
		 *
		 * 思路
		 首先，我们将描述一种查找一组字符串的最长公共前缀 LCP(S_1 \ldots S_n)LCP(S1…Sn) 的简单方法。
		 我们将会用到这样的结论：LCP(S_1 \ldots S_n) = LCP(LCP(LCP(S_1, S_2),S_3),\ldots S_n)LCP(S1…Sn)=LCP(LCP(LCP(S1,S2),S3),…Sn)算法为了运用这种思想，
		 算法要依次遍历字符串 [S_1 \ldots S_n][S1…Sn]，当遍历到第 ii 个字符串的时候，找到最长公共前缀 LCP(S_1 \ldots S_i)LCP(S1…Si)。
		 当 LCP(S_1 \ldots S_i)LCP(S1…Si) 是一个空串的时候，算法就结束了。 否则，在执行了 nn 次遍历之后，算法就会返回最终答案 LCP(S_1 \ldots S_n)LCP(S1…Sn)。
		 * @param strs 比较的数组
		 * @return 相同的字符串
		 *
		 */
		public static String longestCommonPrefix(String[] strs) {
			if (strs == null || strs.length == 0) {
				return "";
			}
			String prefix = strs[0];
			for (int i = 0; i < strs.length; i++) {
				while (strs[i].indexOf(prefix) != 0) {
					prefix = prefix.substring(0, prefix.length() - 1);
					if (prefix.isEmpty()) {
						return "";
					}
				}
			}
			return prefix;
		}
	}
	
	/**
	 * 罗马数字包含以下七种字符： I， V， X， L，C，D 和 M。
	 
	 字符          数值
	 I             1
	 V             5
	 X             10
	 L             50
	 C             100
	 D             500
	 M             1000
	 例如， 罗马数字 2 写做 II ，即为两个并列的 1。12 写做 XII ，即为 X + II 。 27 写做  XXVII, 即为 XX + V + II 。
	 
	 通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做 IIII，而是 IV。数字 1 在数字 5 的左边，所表示的数等于大数 5 减小数 1 得到的数值 4 。同样地，数字 9 表示为 IX。这个特殊的规则只适用于以下六种情况：
	 
	 I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。
	 X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。 
	 C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。
	 给定一个整数，将其转为罗马数字。输入确保在 1 到 3999 的范围内。
	 *
	 */
	public static String intToRoman(int num) {
	
		int[] nums = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
		String[] roman = {"M","CM","D","CD","C","XC","L","XL","X","IC","V","IV","I"};
		StringBuilder stringBuffer = new StringBuilder();
		int index = 0;
		for (;;) {
			while (num >= nums[index]) {
				stringBuffer.append(roman[index]);
				num -= nums[index];
			}
			if (num == 0) {
				break;
			}
			index++;
		}
		System.out.println(stringBuffer);
		return stringBuffer.toString();
	
	}
	
	
}
