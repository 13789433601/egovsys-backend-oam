package com.xzgzs.commen.util;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * @Author: wangyf
 * @Date: 2019/8/13 23:06
 */
@Component
public class HttpClientUtil {
	public static final Logger logger = LoggerFactory.getLogger(HttpClientUtil.class);

	/**
	 * http 无参数请求
	 * @param url 请求地址
	 * @return 请求体
	 * @throws IOException response关闭的异常
	 */
	public String getMethodNoParam(String url) throws IOException {
		//创建httpClient请求
		CloseableHttpClient client = HttpClients.createDefault();
		//创建httpGet请求
		HttpGet httpGet = new HttpGet(url);
		CloseableHttpResponse response = null;
		try {
			//执行请求
			response = client.execute(httpGet);
			//判断状态码是否正确，正确则返回请求实体
			if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
				String entity = EntityUtils.toString(response.getEntity(), "UTH-8");
				logger.info(entity);
			}
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("http请求出错，请重新发送请求");
			return "httpClient请求出错";
		} finally {
			if (response != null) {
				response.close();
			}
			client.close();
		}
		return EntityUtils.toString(response.getEntity(), "UTH-8");
	}

	/**
	 * httpClient GET有参方法。
	 * @param url 请求地址
	 * @param map 参数
	 * @return String类型的数据
	 * @throws URISyntaxException 异常
	 */
	public String getMethodParam(String url, Map<String, Object> map) throws URISyntaxException {
		//创建连接请求
		CloseableHttpClient client = HttpClients.createDefault();

		StringBuffer sb = new StringBuffer();
		Iterator<String> iterator = map.keySet().iterator();
		while (iterator.hasNext()){
			String key = iterator.next();
			String value = map.get(key).toString();
			sb.append("&"+key+"="+value);
		}
		String param = sb.substring(0).replaceFirst("&","");
		//创建get请求
		HttpGet httpGet = new HttpGet(url+"?"+param);
		CloseableHttpResponse response = null;
		String entity = null;
		try {
			response = client.execute(httpGet);
			if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
				entity = EntityUtils.toString(response.getEntity());
				System.out.println(entity);
			}
		} catch (IOException e) {
//			e.printStackTrace();
			return "请求出错";
		}
		return entity;
	}

	/**
	 * httpClient POST 方式调用
	 * @param url 路径
	 * @param map 参数
	 * @return 调用结果，string类型
	 */
	public String postMethod(String url, Map<String, Object> map) {
		//创建httpclient对象
		CloseableHttpClient client = HttpClients.createDefault();
		//创建post对象
		HttpPost httpPost = new HttpPost(url);
		//
		CloseableHttpResponse response = null;
		//给post请求配置参数
		List<NameValuePair> list = new ArrayList<>();
		for (String keys : map.keySet()) {
			list.add(new BasicNameValuePair(keys, String.valueOf(map.get(keys))));
		}
		String entitys = null;
		try {
			//将参数进行编码为合适的格式,如将键值对编码为param1=value1&param2=value2
			UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(list,"utf-8");
			httpPost.setEntity(urlEncodedFormEntity);
			//执行post请求
			response = client.execute(httpPost);

			if (HttpStatus.SC_OK == response.getStatusLine().getStatusCode()) {
				entitys =EntityUtils.toString(response.getEntity());
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return entitys;
	}



}
