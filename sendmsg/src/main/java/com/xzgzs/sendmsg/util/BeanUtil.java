package com.xzgzs.sendmsg.util;

import org.springframework.context.annotation.ComponentScan;

/**
 * @Author: wangyf
 * @Date: 2019/9/28 21:07
 */
@ComponentScan(value = "com.xzgzs.commen")
public class BeanUtil {
}
