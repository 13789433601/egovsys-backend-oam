package com.xzgzs.usermanage.service;

import com.xzgzs.usermanage.dto.UserDTO;

/**
 * @Author: wangyf
 * @Date: 2019/5/26 13:51
 */
public interface UserService {

	/**
	 * 保存注册用户
	 * @param userDTO 用户对象
	 */
	void saveUser(UserDTO userDTO);

}
