package com.xzgzs.sendmsg.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

/**
 * @Author: wangyf
 * @Date: 2019/6/11 18:01
 */
@Component
public class SendMsgUtil {
	/**
	 * 发件人地址
	 */
	@Value("${user.senderAddr}")
	public String senderAddr;
	/**
	 * 收件人地址
	 */
//	@Value("${user.recipientAddr}")
//	public static String recipientAddr;
	/**
	 * 发送人用户名
	 */
	@Value("${user.senderAccount}")
	public  String senderAccount;
	/**
	 * 发送人密码
	 */
	@Value("${user.senderPwd}")
	public  String senderPwd;

	/**
	 * 发送邮件
	 * @param recipientAddr 收件人地址
	 */
	public void sendMsg(String recipientAddr) throws MessagingException {
		//发送邮件服务器的参数配置
		Properties properties = new Properties();
		//用户认证方式
		properties.setProperty("mail.smtp.auth","true");
		//传输协议
		properties.setProperty("mail.transport.protocol","smtp");
		//发件人的smtp服务器地址
		properties.setProperty("mail.smtp.host","smtp.163.com");
		//2创建定义整个应用程序的环境信息Session对象
		Session session = Session.getInstance(properties);
		//设置吊事信息在控制台打印出来
		session.setDebug(true);
		//创建邮件的实例对象
		Message message = getMimeMessage(session, recipientAddr);
		//根据session对象或得邮件的传输对象Transport
		Transport transport = session.getTransport();
		//设置邮件发送人的账号密码
		transport.connect(senderAccount,senderPwd);
		//发送邮件，并发送到所有收件人地址，
		// message.getASllRecipients() 获取到的是在创建邮件对象时添加的所有收件人，抄送人，密送人
		transport.sendMessage(message,message.getAllRecipients());
		//如果只想发送给指定的人，可以如下写法
//		transport.sendMessage(message,new Address[]{new InternetAddress("xxx@qq.com")});
		//关闭邮件连接
		transport.close();
	}

	private Message getMimeMessage(Session session, String recipientAddr) throws MessagingException {

		//设置一封邮件的实例对象
		MimeMessage mimeMessage = new MimeMessage(session);
		//发件人地址
        mimeMessage.setFrom(new InternetAddress(senderAddr));
		/**
		 * 设置收件人地址，（可以增加多个收件人，抄送，密送）
		 * MimeMessage.RecipientType.TO 发送
		 * MimeMessage.RecipientType.CC 抄送
		 * MimeMessage.RecipientType.BCC 密送
		 */
		mimeMessage.setRecipients(MimeMessage.RecipientType.TO,
				new InternetAddress[]{new InternetAddress(recipientAddr)});
		//设置邮件主题
		mimeMessage.setSubject("测试邮件","UTF-8");
		//设置正文
		mimeMessage.setContent("文本文件","text/html;charset=UTF-8");
		//设置邮件的发送时间，默认立即发送
		mimeMessage.setSentDate(new Date());
		return mimeMessage;
	}

}
