package com.xzgzs.commen.annotion.impl;

import com.xzgzs.commen.annotion.ErrorLoggerPrint;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;

/**
 * @Author: wangyf
 * @Date: 2019/9/27 22:39
 */
@Aspect
@Component
public class ErrorLoggerPrintImpl {
	private Logger logger = LoggerFactory.getLogger(ErrorLoggerPrint.class);
	@Pointcut("@annotation(com.xzgzs.commen.annotion.ErrorLoggerPrint)")
	private void cut () {

	}
	@Before("cut()")
	public void around(JoinPoint joinPoint) throws IOException {
		String value = getValue(joinPoint);
		File file =new File("D:\\logger.txt");
		Writer out =new FileWriter(file);
		out.write(value);
		out.close();
	}

	@After("cut()")
	public void afterAround(JoinPoint joinPoint) {
		String value = getValue(joinPoint);
		System.out.println("结束"+value);
		logger.error("结束"+value);
	}
	public String getValue(JoinPoint joinPoint) {
		MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
		Method method = methodSignature.getMethod();
		ErrorLoggerPrint annotation = method.getAnnotation(ErrorLoggerPrint.class);
		String value = annotation.value();
		return value;
	}
}
